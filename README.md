# Commander-Class-On-J2ME
import javaxmicroedition.midlet. *;
import javax.microedition.lcdui.*;

public class Commander extends MIDlet {
   public void startapp() {
	 Displayable d = new TextBox("TextBox", "Commander",20, TextField.ANY);
	 Command c = new Command("Exit", Command.EXIT,0);
	 d.addCommand(c);
	 dsetCommandListener(new CommandListener(){
		public void CommandAction(Command c, Displayable s)
		 notifyDestroyed();
	    }
	});
	
	   Display.getDisplay(this).setCurrent(d);
       }
       
       public void pauseApp(){}
	       
       public void destoroyApp(boolean unconditional){}
   }
